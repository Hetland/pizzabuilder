const availableToppings = [
    {
        name: 'Cheese',
        price: 30,
    },
    {
        name: 'Mushroom',
        price: 50
    },
    {
        name: 'Pepperoni',
        price: 75
    },
    {
        name: 'Pineapple',
        price: 35
    }
];

// New Builder Class using the Module Design Pattern.
const PizzaBuilder = function (appId) {

    // References to reused DOM Elements.
    const elApp      = document.getElementById(appId);
    const elPizza    = elApp.children.namedItem('pizza-container').children.namedItem('pizza');
    const elToppings = elApp.children.namedItem('toppings');
    const elToppingActions = elToppings.children.namedItem('topping--actions');
    const elOrder    = elApp.children.namedItem('order');
    const elOrderTotal = document.getElementById('order--total');
    const elOrderToppings = document.getElementById('order--toppings');

    // Verify child elements.
    if (!elPizza || !elToppings || !elOrder) {
        throw new Error("Please ensure you have elements with id's 'pizza', 'toppings' and 'order' inside your app element.");
    }

    // A list of toppings currently on the pizza.
    let currentToppings = [];

    // Helper Objects - IIFE Design Pattern
    const orderManager = (function() {

        let total = 0;

        return {
            update() {
                total = 80;
                currentToppings.forEach((topping, index, arr) => {
                    total += topping.price;
                });
                elOrderTotal.textContent = total.toPrecision(3);
                this.updateToppingList();
            },
            updateToppingList() {
               elOrderToppings.innerHTML = "";
               currentToppings.forEach((topping, index, arr) => {
                   let listItemTopping = document.createElement('li');
                   listItemTopping.textContent = `${topping.name}-${topping.price.toFixed(2)}`;
                   elOrderToppings.appendChild(listItemTopping);
               });

               if (currentToppings.length == 0) {
                let noToppingsAdded = document.createElement('li');
                noToppingsAdded.textContent = "No toppings added";
                elOrderToppings.appendChild(noToppingsAdded);
               }
            }
        }
    })();
    const toppingManager = (function () {
        return {
            hasTopping(topping) {
              //better to use (topping in currentToppings)?
              let hasSpecificTopping = currentToppings.find((toppingOnPizza) => {
                topping === toppingOnPizza;
              });
              if (hasSpecificTopping === topping) {
                  return true;
              }
              return false;
            },
            add(topping) {
               currentToppings.push(topping);
               let newTopping = document.createElement('div');
               newTopping.className = `topping topping--${topping.name.toLowerCase()}`;
               console.log();
               elPizza.appendChild(newTopping);
               orderManager.update();
            },
            remove(topping) {
                currentToppings = currentToppings.filter(toppingOnPizza => toppingOnPizza !== topping);
                let toppingToRemove = document.querySelector(`.topping--${topping.name.toLowerCase()}`);
                toppingToRemove.parentNode.removeChild(toppingToRemove);
            }
        }
    }());

    return {
        /**
         * Create a basic Pizza Base
         * @returns {Builder}
         */
        createBase() {
            let pizzaBottom = document.createElement('div');
            pizzaBottom.className = "pizza-base";
            elPizza.appendChild(pizzaBottom);
            orderManager.update();
            // DO NOT REMOVE THE RETURN.
            return this;
        },
        /**
         * Create the actions for all the toppings in the Topping list.
         * @returns {Builder}
         */
        createToppingActions() {
            availableToppings.forEach((topping, index, arr) => {
                let toppingBtn = document.createElement('button');
                toppingBtn.textContent = topping.name;
                toppingBtn.onclick = () => {
                    this.toggleTopping(topping);
                };
                elToppingActions.appendChild(toppingBtn);
            });
            // DO NOT REMOTE THE RETURN.
            return this;
        },
        /**
         * Add or Remove a topping.
         * Uses the toppingManager object.
         * @param topping
         */
        toggleTopping(topping) {
            if (currentToppings.includes(topping)) {
                toppingManager.remove(topping);
            } else {
                toppingManager.add(topping)
            }
            orderManager.update()
        }
    }
};
